import React from "react"
import Navbar from "./components/Navbar";
import Main from "./components/Main"
import Project from "./components/Project";
import Footer from "./components/Footer";
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";

export default function App()
{
  return(
  <main>
    <Router>
    <Navbar/>
      <Routes>
          <Route
            exact
            path="/"
            element={<Main/>}
          />
          <Route
              path="/project"
              element={<Project />}
          />
      </Routes>
    </Router>
    <Footer/>
  </main>
  );
}
