// src/components/Navbar.js
import React from "react";
import { useLocation } from "react-router-dom";
// Navbar data
const navbarData = [
    {dataName: "About", href: "#about"}, 
    {dataName: "Portfolio", href:"#portfolio"},
    {dataName: "Skills", href: "#skills"}, 
    {dataName: "Contact", href: "#contact"}];

export default  function Navbar(){
    const location = useLocation();
return (
    
    <nav className="navbar navbar-expand-lg bg-secondary fixed-top" id="mainNav">
        <div className="container">
            <a className="navbar-brand" href="/">
                {/*<img  src='../assets/images/cloud-network.gif' alt="Portfolio Lena"></img>*/}
                    Portfolio Lena
            </a>
            
            {/* display navbar content based on location */}
            {location.pathname==='/' ? <>
            <button className="navbar-toggler 
                                font-weight-bold 
                                bg-primary 
                                text-white 
                                rounded" type="button" 
                    data-bs-toggle="collapse" 
                    data-bs-target="#navbarResponsive" 
                    aria-controls="navbarResponsive" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation">
                        Menu
                    <i className="fas fa-bars ms-1"></i>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
                <ul className="navbar-nav ms-auto">
                    {navbarData.map((data) => 
                    <li key={data.href} className="nav-item mx-0 mx-lg-1">
                        <a className="nav-link py-3 px-0 px-lg-3 rounded" href={data.href}>
                            {data.dataName}
                        </a>
                    </li>)
                    }
                    </ul>
            </div></> : null}
        </div>
    </nav>
);
}

