//./component/Project

import React from "react";
import { Link } from "react-router-dom"
//Project code page

export default function Project()
{

    return (
        <>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <div class="text-center mt-4">
            <Link to="https://gitlab.com/nflema/portfolio_app" className="btn btn-secondary btn-outline-light">
                Source Code
            </Link>
            <Link to="/" className="btn btn-secondary btn-outline-light">
                Back to main
            </Link>
            </div>
            <div className="card card-body">
                <div className="container ">
                    <div className="row align-items-center">
                        <div className="featured-text text-center text-lg-center">
                            <h4>Project description</h4>
                            <p class="text-black-50 mb-0">Lorem ipsum dolor sit amet, consectetur 
                            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore 
                            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                            deserunt mollit anim id est laborum.</p>
                        </div> 
                    </div>
                </div>
            </div>
        </>
    );

}
