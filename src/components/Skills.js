// src/components/Skills.js
/* Skills section contains list of skills that the author
 * has knowledge of and by clicking on each skill button
 * user can see a short description of the author's skill */

import React, {useState} from "react"
import skillSet from "../json/skills.json"

export default function Skills()
{
    //changing the state of the button from closed to open by id
    let [selectedId, setSelectedId] = useState();

    return(
        // Skills section
    <section className="page-section bg-primary text-white mb-0" id="skills">
            <div className="container"></div>
            <h2 className="page-section-heading 
                           text-center 
                           text-uppercase 
                           text-white">Skills</h2> 
            <div className="text-center mt-4">
                {/* Adding skills from json file */}
                {
                skillSet.map((data) => { return(
                <ReactiveButton key={data.id} 
                value={data} 
                isOpen={data.id === selectedId}
                onSelect={() => selectedId !== data.id ? 
                    setSelectedId(data.id) : setSelectedId()}/>
                );
                })}    
                </div>
    </section>  
    );
}

//function to generate specific button using the vals 
//from json and check if button is closed or open
function ReactiveButton({value, isOpen, onSelect})
{  
    return(
    <>
        <button className="btn btn-sm btn-outline-light rounded-4" 
        onClick={ onSelect}>
            {/* skill icon */}
            <img src={`${value.skillImage}`} alt="" className="icon" ></img>
            <br></br>
            {/* skill name */}
            {value.skillName}
            {/* skill description, initial state: hidden */}
            {isOpen ? (<p>Text goes here!</p>) : null}
        </button>
         &nbsp; 
         </>);
}