// ./src/components/About.js
/* * About section contains information about the author
   * description of work and personal aspirations */
export default function About(){
    return (
        // About section
        <section className="page-section bg-primary text-white mb-1" 
                 id="about">
            <div className="container">
               { /* About Section Heading*/}
               <br></br>
               <br></br>
                <h2 className="page-section-heading 
                text-center 
                text-uppercase 
                text-white"> 
                About</h2>
                {/* Icon Divider*/}
                <div className="divider-custom divider-light">
                    <div className="divider-custom-line"></div>
                    <div className="divider-custom-icon">
                        <i className="fas fa-star"></i></div>
                    <div className="divider-custom-line"></div>
                </div>
               { /* About Section Content*/}
                <div className="row">
                    <div className="col-lg-4 ms-auto text-center">
                        <p className="lead">My name is Lena</p></div>
                    <div className="col-lg-4 me-auto text-center">
                        <p className="lead">I am a software developer</p></div>
                </div>
            </div>
        </section>
);
}