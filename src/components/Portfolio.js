// src/components/Portfolio
import React from "react"
import {Link} from "react-router-dom"

export default function Portfolio()
{
    return (
        // Portfolio Section
        <section className="page-section portfolio" id="portfolio">
            <div className="container">
               { /* Portfolio Section Heading */}
                <h2 className="page-section-heading 
                text-center 
                text-uppercase 
                text-secondary mb-0">Portfolio</h2>
               { /* Icon Divider */}
                <div className="divider-custom">
                    <div className="divider-custom-line"></div>
                    <div className="divider-custom-icon"><i className="fas fa-star"></i></div>
                    <div className="divider-custom-line"></div>
                </div>
            {/* Portfolio Grid Items */}
                <div className="row justify-content-center">
                    {/* Portfolio Item 1 */}
                    <div className="col-md-6 col-lg-4 mb-5">
                            <Link  to='/project' className="card card-body">
                                <img className="card-img"
                                src="../assets/images/skill_images/c_icon_132529.png" alt="..." />
                            </Link>
                    </div>
                </div>
            </div>
        </section>

    );
}
