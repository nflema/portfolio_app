//./components/Main
// Main page 
import React from "react"
import About from "./About";
import Portfolio from "./Portfolio";
import Skills from "./Skills";
import Contact from "./Contact";


export default function Main()
{
    return (
        <main>
            <About/>
            <Portfolio/>
            <Skills/>
            <Contact/>
        </main>
    );
}